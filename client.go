package gomailerlite

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
)

const (
	apiBase        = "https://api.mailerlite.com"
	contentTypeKey = "Content-Type"
	contentType    = "application/json"
	authHeaderKey  = "X-MailerLite-ApiKey"
)

// Client manages the interactions with the mailerlite api
type Client struct {
	apiKey     string
	httpClient *http.Client
}

// NewClient returns a pointer to a new client with the given api key
func NewClient(apiKey string) *Client {
	c := &Client{apiKey: apiKey}
	c.httpClient = &http.Client{}
	return c
}

// NewSubscriber adds a new subscriber
func (c *Client) NewSubscriber(email, name string, fields map[string]interface{}) (Subscriber, error) {
	sub := newsubscriber{
		Name:   name,
		Email:  email,
		Fields: fields,
	}
	var buf bytes.Buffer
	err := json.NewEncoder(&buf).Encode(sub)
	if err != nil {
		return Subscriber{}, err
	}

	req, err := c.initPost("/api/v2/subscribers", &buf)
	if err != nil {
		return Subscriber{}, err
	}

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return Subscriber{}, err
	}

	if resp.StatusCode != 200 {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return Subscriber{}, fmt.Errorf("could not read error body")
		}
		resp.Body.Close()
		return Subscriber{}, fmt.Errorf("request not successful: " + string(body))
	}

	var subscriber Subscriber
	err = json.NewDecoder(resp.Body).Decode(&subscriber)
	return subscriber, err
}

// NewSubscriberToGroup adds a new subscriber to the given group
func (c *Client) NewSubscriberToGroup(email, name string, fields map[string]interface{}, groupId int) (Subscriber, error) {
	sub := newsubscriber{
		Name:   name,
		Email:  email,
		Fields: fields,
	}

	var buf bytes.Buffer
	err := json.NewEncoder(&buf).Encode(sub)
	if err != nil {
		return Subscriber{}, err
	}

	req, err := c.initPost(fmt.Sprintf("/api/v2/groups/%d/subscribers", groupId), &buf)
	if err != nil {
		return Subscriber{}, err
	}

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return Subscriber{}, err
	}

	var subscriber Subscriber
	err = json.NewDecoder(resp.Body).Decode(&subscriber)
	return subscriber, err
}

func (c *Client) initGet(subPath string) (*http.Request, error) {
	return c.initRequest("GET", subPath, nil)
}

func (c *Client) initPost(subPath string, body io.Reader) (*http.Request, error) {
	return c.initRequest("POST", subPath, body)
}

func (c *Client) initRequest(method, subPath string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequest(method, apiBase+subPath, body)
	if err != nil {
		return req, err
	}
	req.Header.Add(authHeaderKey, c.apiKey)
	req.Header.Add(contentTypeKey, contentType)

	return req, nil
}

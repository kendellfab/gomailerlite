package gomailerlite

type newsubscriber struct {
	Name   string `json:"name"`
	Email  string `json:"email"`
	Fields map[string]interface{} `json:"fields"`
}

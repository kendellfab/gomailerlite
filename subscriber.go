package gomailerlite

// Subscriber represents a mailerlite subscriber
type Subscriber struct {
	ID              int        `json:"id"`
	Name            string     `json:"name"`
	Email           string     `json:"email"`
	Sent            int        `json:"sent"`
	Opened          int        `json:"opened"`
	Clicked         int        `json:"clicked"`
	Type            string     `json:"type"`
	Fields          []Field    `json:"fields"`
	/*DateSubscribe   *time.Time `json:"date_subscribe"`
	DateUnsubscribe *time.Time `json:"date_unsubscribe"`
	DateCreated     *time.Time `json:"date_created"`
	DateUpdated     *time.Time `json:"date_updated"`*/
}

type Field struct {
	Key   string `json:"key"`
	Value string `json:"value"`
	Type  string `json:"type"`
}
